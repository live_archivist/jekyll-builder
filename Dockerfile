FROM jekyll/builder

WORKDIR /usr/src/app
COPY Gemfile ./
RUN bundler install
COPY . .
